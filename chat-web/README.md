# Chat en Tiempo Real con WebSocket y React para el Hacktoberfest de Python Coruña

Este proyecto es una aplicación de chat en tiempo real desarrollada como parte del evento Hacktoberfest de Python Coruña. La aplicación utiliza tecnologías web modernas, incluyendo WebSocket para la comunicación en tiempo real y React para el frontend. La aplicación permite a los usuarios enviar mensajes en tiempo real y participar en conversaciones interactivas.

## Requisitos

- **Node.js y npm:** Asegúrate de tener Node.js y npm instalados en tu sistema para ejecutar la aplicación React.
- **Python 3:** Necesitarás Python 3 para ejecutar el servidor WebSocket.

## Configuración del Proyecto

1. **Clonar el Repositorio:**

    ```bash
    git clone https://github.com/tu-usuario/chat-websocket-react.git
    cd chat-websocket-react
    ```

2. **Instalar Dependencias Frontend:**

    ```bash
    cd frontend
    npm install
    ```

3. **Instalar Dependencias Backend (Python):**

    ```bash
    cd backend
    pip install -r requirements.txt
    ```

## Ejecución del Proyecto

1. **Ejecutar el Backend (WebSocket en Python):**

    ```bash
    cd backend
    python websocket_server.py
    ```

2. **Ejecutar el Frontend (React):**

    ```bash
    cd frontend
    npm start
    ```

## Uso de la Aplicación

Una vez que hayas seguido los pasos anteriores, puedes acceder a la aplicación de chat en tiempo real desde tu navegador. La aplicación te permitirá enviar mensajes en tiempo real a otros usuarios conectados al mismo chat.

## Contribuciones para el Hacktoberfest

Este proyecto es parte del Hacktoberfest de Python Coruña. Si deseas contribuir, asegúrate de leer las pautas de contribución en el repositorio. ¡Esperamos tus contribuciones y disfruta del Hacktoberfest!

¡Disfruta chateando en tiempo real con WebSocket y React durante el Hacktoberfest de Python Coruña!
